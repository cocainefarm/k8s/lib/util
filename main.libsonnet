local util(k) = {
  web:: (import 'web.libsonnet').withK(k),
  rbac:: (import 'rbac.libsonnet').withK(k),
  cilium:: (import 'cilium.libsonnet').withK(k),

  inlineSpec(apiserver, namespace, envSlug=null, projectPathSlug=null):: {
    apiVersion: 'tanka.dev/v1alpha1',
    kind: 'Environment',
    metadata: {
      name: namespace,
    },
    spec: {
      apiServer: apiserver,
      namespace: namespace,
      resourceDefaults: {
        annotations+:
          if projectPathSlug != null then { 'app.gitlab.com/app': projectPathSlug }
          + if envSlug != null then { 'app.gitlab.com/env': envSlug },
      },
      injectLabels: true,
    },
  },

  injectGlobalAnnotations()::
    k.apps.v1.statefulSet.spec.template.metadata.withAnnotationsMixin(
      $.spec.resourceDefaults.annotations),

  serviceFor(deployment, ignored_labels=[], nameFormat='%(port)s')::
    local container = k.core.v1.container;
    local service = k.core.v1.service;
    local servicePort = k.core.v1.servicePort;
    local ports = [
      servicePort.newNamed(
        name=(nameFormat % { container: c.name, port: port.name }),
        port=port.containerPort,
        targetPort=port.containerPort
      ) +
      if std.objectHas(port, 'protocol')
      then servicePort.withProtocol(port.protocol)
      else {}
      for c in deployment.spec.template.spec.containers
      for port in (c + container.withPortsMixin([])).ports
    ];
    local labels = {
      [x]: deployment.spec.template.metadata.labels[x]
      for x in std.objectFields(deployment.spec.template.metadata.labels)
      if std.count(ignored_labels, x) == 0
    };

    service.new(
      deployment.metadata.name,  // name
      labels,  // selector
      ports,
    ) +
    service.mixin.metadata.withLabels({ name: deployment.metadata.name }),

  httpProbes(port, path, livenessPeriod, livenessDelay, readinessPeriod, readinessDelay)::
    local container = k.core.v1.container;

    container.livenessProbe.httpGet.withPort(port)
    + container.livenessProbe.httpGet.withPath(path)
    + container.livenessProbe.withPeriodSeconds(livenessPeriod)
    + container.livenessProbe.withInitialDelaySeconds(livenessDelay)
    + container.readinessProbe.httpGet.withPort(port)
    + container.readinessProbe.httpGet.withPath(path)
    + container.readinessProbe.withPeriodSeconds(readinessPeriod)
    + container.readinessProbe.withInitialDelaySeconds(readinessDelay),

  commandProbes(command, livenessPeriod, livenessDelay, readinessPeriod, readinessDelay)::
    local container = k.core.v1.container;

    container.livenessProbe.exec.withCommand(command)
    + container.livenessProbe.withPeriodSeconds(livenessPeriod)
    + container.livenessProbe.withInitialDelaySeconds(livenessDelay)
    + container.readinessProbe.exec.withCommand(command)
    + container.readinessProbe.withPeriodSeconds(readinessPeriod)
    + container.readinessProbe.withInitialDelaySeconds(readinessDelay),

  tcpProbes(port, livenessPeriod, livenessDelay, readinessPeriod, readinessDelay)::
    local container = k.core.v1.container;

    container.livenessProbe.tcpSocket.withPort(port)
    + container.livenessProbe.withPeriodSeconds(livenessPeriod)
    + container.livenessProbe.withInitialDelaySeconds(livenessDelay)
    + container.readinessProbe.tcpSocket.withPort(port)
    + container.readinessProbe.withPeriodSeconds(readinessPeriod)
    + container.readinessProbe.withInitialDelaySeconds(readinessDelay),

  mapVolumeClaimTemplates(func):: {
    local volumes = super.spec.volumeClaimTemplates,

    spec+: {
      volumeClaimTemplates: std.map(func, volumes)
    }
  },

  volumeClaimTemplate:: {
    new(name, size):: {
      metadata: {
        name: name,
      },
      spec: {
        resources: {
          requests: {
            storage: size,
          },
        },
        accessModes: [
          'ReadWriteOnce',
        ],
      },
    },

    withName(name):: {
      metadata+: {
        name: name,
      },
    },

    withAccessModes(accessModes):: {
      spec+: {
        accessModes: accessModes,
      },
    },

    withStorageClass(storageClass):: {
      spec+: {
        storageClassName: storageClass,
      },
    },

    withStorageRequests(request):: {
      spec+: {
        resources+: {
          requests+: {
            storage: request,
          },
        },
      },
    },
  },


  pvcVolumeMount(name, pvcName, path, readOnly=false, volumeMountMixin={})::
    local container = k.core.v1.container,
          deployment = k.apps.v1.deployment,
          volumeMount = k.core.v1.volumeMount,
          volume = k.core.v1.volume,
          addMount(c) = c + container.withVolumeMountsMixin(
      volumeMount.new(name, path, readOnly=readOnly) +
      volumeMountMixin,
    );

    deployment.mapContainers(addMount) +
    deployment.mixin.spec.template.spec.withVolumesMixin([
      volume.fromPersistentVolumeClaim(name, pvcName),
    ]),

  ingressFor(target, domain, tlsSecret, acme=false)::
    local ingress = k.networking.v1.ingress,
          ingressRule = k.networking.v1.ingressRule,
          ingressTLS = k.networking.v1.ingressTLS,
          httpIngressPath = k.networking.v1.httpIngressPath,
          service = k.core.v1.service;

    ingress.new(target.metadata.name) +
    ingress.mixin.metadata.withAnnotationsMixin({
      'kubernetes.io/tls-acme': std.toString(acme),
    }) +
    ingress.mixin.spec.withRules(
      ingressRule.withHost(domain) +
      ingressRule.mixin.http.withPaths(
        httpIngressPath.withPath('/') +
        httpIngressPath.withPathType('Prefix') +
        httpIngressPath.backend.service.withName(target.metadata.name) +
        httpIngressPath.backend.service.port.withName(target.spec.ports[0].name)
      )
    ) +
    ingress.mixin.spec.withTls(
      ingressTLS.withHosts(domain) +
      ingressTLS.withSecretName(tlsSecret)
    ),

  image(image)::
    image.repo + ':' + image.tag,
};

util((import 'k.libsonnet'))
