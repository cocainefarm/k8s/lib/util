local web(k) = {
  local service = k.core.v1.service,

  local ingress = k.networking.v1.ingress,
  local ingressRule = k.networking.v1.ingressRule,
  local ingressTLS = k.networking.v1.ingressTLS,
  local httpIngressPath = k.networking.v1.httpIngressPath,

  certificate:: {
    new(name, secret, domains):: {
      apiVersion: 'cert-manager.io/v1',
      kind: 'Certificate',
      metadata: {
        name: name,
      },
      spec: {
        issuerRef: {
          group: 'cert-manager.io',
          kind: 'ClusterIssuer',
          name: 'cocainedns',
        },
        dnsNames: domains,
        secretName: secret,
      },
    },

    withIssuer(group, kind, name):: {
      spec+: {
        issuerRef: {
          group: group,
          kind: kind,
          name: name,
        }
      }
    }
  },

  serveS3(
    domain
    , path
    , pathType='Prefix'
    , s3Target="/public/%s/$1" % domain
    , useRegex="true"
    , tlsSecret='external-s3'
    , name='external-s3'
    , s3Url='minio.minio.svc.cluster.kube.cat'
    , s3Port=9000
  ):: {
    service:
      service.new('external-s3', {}, [])
      + service.spec.withType('ExternalName')
      + service.spec.withExternalName(s3Url),
    ingress:
      ingress.new(name) +
      ingress.metadata.withAnnotationsMixin({
        "nginx.ingress.kubernetes.io/rewrite-target": s3Target,
        "nginx.ingress.kubernetes.io/upstream-vhost": "%s:%s" % [s3Url, s3Port],
        "nginx.ingress.kubernetes.io/use-regex": useRegex,
        "nginx.ingress.kubernetes.io/configuration-snippet": |||
          rewrite ^/$ /index.html;
          rewrite ^(.*)/$ $1/index.html;
        |||,
      }) +
      ingress.mixin.spec.withRules(
        ingressRule.withHost(domain) +
        ingressRule.mixin.http.withPaths(
          httpIngressPath.withPath(path) +
          httpIngressPath.withPathType(pathType) +
          httpIngressPath.backend.service.withName(self.service.metadata.name) +
          httpIngressPath.backend.service.port.withNumber(s3Port)
        )
      ) +
      ingress.mixin.spec.withTls(
        ingressTLS.withHosts(domain) +
        ingressTLS.withSecretName(tlsSecret)
      ),
  },
};

web((import 'k.libsonnet')) + {
  withK(k):: web(k),
}
