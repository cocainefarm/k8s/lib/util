local cilium(k) = {
  egressNatPolicy:: {
    podSelector:: k.networking.v1.networkPolicy.spec.podSelector,

    new(name):: {
        kind: 'CiliumEgressNATPolicy',
        apiVersion: 'cilium.io/v2alpha1',
        metadata: {
          name: name,
        },
    },

    withEgressSourceIP(ip):: {
      spec+: {
        egressSourceIP: ip,
      },
    },

    withDestinationCIDRs(cidrs):: {
      spec+: {
        destinationCIDRs: cidrs,
      },
    },

    withPodSelector(selector):: {
      spec+: {
        egress: [selector.spec],
      },
    },
  }
};

cilium((import 'k.libsonnet')) + {
  withK(k):: cilium(k),
}
