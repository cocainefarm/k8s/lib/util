local rbac(k) = {
  // rbac creates a service account, role and role binding with the given
  // name and rules.
  rbac(name, rules, namespace):: {
    local clusterRole = k.rbac.v1.clusterRole,
    local clusterRoleBinding = k.rbac.v1.clusterRoleBinding,
    local subject = k.rbac.v1.subject,
    local serviceAccount = k.core.v1.serviceAccount,

    service_account:
      serviceAccount.new(name) +
      serviceAccount.mixin.metadata.withNamespace(namespace),

    cluster_role:
      clusterRole.new(name) +
      clusterRole.withRules(rules),

    cluster_role_binding:
      clusterRoleBinding.new(name) +
      clusterRoleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
      clusterRoleBinding.mixin.roleRef.withKind('ClusterRole') +
      clusterRoleBinding.mixin.roleRef.withName(name) +
      clusterRoleBinding.withSubjects([
        subject.fromServiceAccount(self.service_account),
      ]),
  },

  namespacedRBAC(name, rules, namespace):: {
    local role = k.rbac.v1.role,
    local roleBinding = k.rbac.v1.roleBinding,
    local subject = k.rbac.v1.subject,
    local serviceAccount = k.core.v1.serviceAccount,

    service_account:
      serviceAccount.new(name) +
      serviceAccount.mixin.metadata.withNamespace(namespace),

    role:
      role.new(name) +
      role.mixin.metadata.withNamespace(namespace) +
      role.withRules(rules),

    role_binding:
      roleBinding.new(name) +
      roleBinding.mixin.metadata.withNamespace(namespace) +
      roleBinding.mixin.roleRef.withApiGroup('rbac.authorization.k8s.io') +
      roleBinding.mixin.roleRef.withKind('Role') +
      roleBinding.mixin.roleRef.withName(name) +
      roleBinding.withSubjects([
        subject.fromServiceAccount(self.service_account),
      ]),
  },
};

rbac((import 'k.libsonnet')) + {
  withK(k):: rbac(k),
}
